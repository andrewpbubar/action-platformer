# Importing different Libarries to use their code
import pygame  # using a short hand name
import sys

# my code
from scripts.utils import load_image
from scripts.entities import PhysicsEntity

# making a game class
class Game:

    def __init__(self):
        pygame.init()

        # setting the vars
        self.win_title = 'Action Platformer'
        self.win_size = (640, 480)
        self.fps = 60

        # setting up the clock
        self.clock = pygame.time.Clock()

        # making the window
        # TODO: Reowork display later
        pygame.display.set_caption(self.win_title)
        self.win = pygame.display.set_mode(self.win_size)

        # colors used with RGB values
        self.black = (0, 0, 0)
        self.baby_blue = (14, 219, 248)
        self.red = (255, 0, 0)
        self.green = (0, 255, 0)
        self.blue = (0, 0, 255)

        # for movement
        self.movement = [False, False, False, False]  # checking if moving in a direction
        self.movement_keys = [pygame.K_LEFT, pygame.K_RIGHT, # Setting the movement keys
                              pygame.K_UP, pygame.K_DOWN]  # in a table
        self.movement_speed = 10

        # setting the asset libarry
        self.assets = {
            "player": load_image('entities/player.png')
        }

        # applying the physics entity to make a player
        self.player = PhysicsEntity(self, 'player', (50, 50), (8, 15))

    def running(self):

        # a infinate loop for the game to run in
        while True:
            # background drawing
            self.win.fill(self.baby_blue)

            # ************************************
            #           Updating Entities
            # ************************************
            self.player.update((self.movement[1] - self.movement[0],
                    self.movement[3] - self.movement[2] ))

            # ************************************
            #             Collision!
            # ************************************

            # ***********************************
            #           Rendering Images
            # ***********************************

            self.player.render(self.win)

            # drawing the img at the top of every frame
            # self.win.blit(self.img, self.img_pos)
            # draws self.img to win at the img_pos position
            # NOTE about blit. it just draws a pygame surface onto another. so an
            # image can be blit onto an image :D

            # ****************************************
            #               Inputs
            # *****************************************

            # checking for all the events pygame sees
            # and naming the result ev
            for ev in pygame.event.get():

                # if the event type is quit (as in the x in the window bar)
                if ev.type == pygame.QUIT:
                    self.close()

                # if the key is down
                if ev.type == pygame.KEYDOWN:

                    # for easy closing of the game
                    if ev.key == pygame.K_ESCAPE:
                        self.close()

                    # setting up movement TODO: there has to be a better way to do this

                    #
                    if ev.key == self.movement_keys[0]:
                        self.movement[0] = True
                    if ev.key == self.movement_keys[1]:
                        self.movement[1] = True
                    if ev.key == self.movement_keys[2]:
                        self.movement[2] = True
                    if ev.key == self.movement_keys[3]:
                        self.movement[3] = True

                        # if the key is up
                if ev.type == pygame.KEYUP:

                    # setting up movement
                    if ev.key == self.movement_keys[0]:
                        self.movement[0] = False
                    if ev.key == self.movement_keys[1]:
                        self.movement[1] = False
                    if ev.key == self.movement_keys[2]:
                        self.movement[2] = False
                    if ev.key == self.movement_keys[3]:
                        self.movement[3] = False

                    for x in self.movement:
                        if ev.key == self.movement_keys[x]:
                            self.movement[x] = False

            # update the dislay
            pygame.display.update()
            self.clock.tick(60)

    def close(self):  # Making closing the game easier
        pygame.quit()  # closing pygame functions
        sys.exit()  # shutting down the program just in case


Game().running()
