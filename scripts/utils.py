
# to make it brain dead easy to load imaged where-ever they are needed
# without having to type out the whole address everytime

import pygame

# the short relative path from main.py to the images
DEMO_IMG_PATH = 'demo_art/data/images/'
    
def load_image(path):
    
    # takes the demo_img_path and adds path to it to find the file :)
    img = pygame.image.load(DEMO_IMG_PATH + path).convert() # TODO: can be better
    img.set_colorkey((0, 0, 0)) # removes black from the image so its focuesd on the actual sprite
    return img # sends back the img to where ever the function is called