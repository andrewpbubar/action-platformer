
import pygame

class PhysicsEntity:
    
    def __init__(self, game, entity_type, position, size):
        
        self.g = game  # to link to things in the game function
        self.type = entity_type  # TODO: for later so I can code less to do more :)
        self.pos = list(position)  # Will learn more about lists, but this is to track where entity is
        self.size = size  # how big is it
        self.vel = [0, 0]  # how fast it be zoomin

    def update(self, movement=(0, 0)):  # should be called every frame
        # how much movement per frame
        frame_movement = (movement[0] + self.vel[0], movement[1], self.vel[1])
        
        # Actually move the entity NOTE: Must be seperate, even in three diamentions
        self.pos[0] += frame_movement[0]
        self.pos[1] += frame_movement[1]

    def render(self, surface):
        surface.blit(self.g.assets['player'], self.pos) # pulling the assets from the game.assets table in Game